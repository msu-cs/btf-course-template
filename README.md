# TODO-Number: TODO-Name, TODO-semester #

This repository is for class materials for TODO-courseinfo, taught by Prof. Fasy.

Course Catalog Description: TODO: Pick one of the following:
(TODO: CSCI 232) PREREQUISITE: CSCI 132. Advanced data structures and programming techniques and their application. Topics include: trees, balanced trees, graphs, dictionaries, hash tables, heaps. Examines the efficiency and correctness of algorithms. The laboratory uses Java. CSCI 246 is recommended as a prerequisite
(TODO: CSCI 432) PREREQUISITE: CSCI 246 and CSCI 232. A rigorous examination of advanced algorithms and data structures. Topics include average case analysis, probabilistic algorithms, advanced graph problems and theory, distributed and parallel programming. CSCI 338 is recommended as a prerequisite  
(TODO: CSCI 535)
(TODO: CSCI 535) Provides an introduction to topological data analysis (TDA). This course will cover the topological, geometric, and algebraic tools used in TDA. Specific topics covered include persistent homology, Reeb graphs, and minimum homotopy area. Students will explore a data set of their choice in a course project, and learn how to apply the tools discussed in lecture.

(TODO:246 only) From the Instructor: This course is NOT a programming class, and is not
structured like the 100- and 200-level courses that precede it.  In this course, we will
do many proofs (especially using induction), and will be writing pseudo-code, not
code.

(TODO:432 only) From the Instructor: This course is NOT a programming class, and is not
structured like the 132 and 232 courses that precede it.  In this course, we will
do many proofs (especially using induction), and will be writing pseudo-code, not
code.

(TODO: 532 only)  





Prerequisites:

(TODO: CSCI 246)

(TODO: CSCI 432)
CSCI 246 (Discrete) and CSCI 232 (Data Structures and
Algorithms) are a prerequisite for this course.
In particular, a student enrolled in CSCI 432
should be familiar with
sorting and searching algorithms, big-Oh notation,
basic recurrence relations,
heaps, queues, lists, hash tables,
proof by induction and by contradiction, and discrete probability.

(TODO: CSCI 532)

(TODO: 535 only) This course assumes that you are familiar with the following topics: set theory, vector spaces,
runtime analysis, and basic data structures. A student in this class should be familiar with proof
techniques, including proof by induction and proof by contradiction. Although not necessary, a
suggested prerequesite for this class is either M 461 (Topology), which is offered every other
Fall (odd years), or CSCI 432 (Algorithms), which is offered every fall.


## Course Outcomes and Objectives

By the end of this course, a student will:

TODO: for CSCI 246, add:
- Be able to use formal proof techniques, including mathematical induction and proof by contradiction.
- Understand algorithmic complexity and be able to use it to compare different program designs for a problem.
- Solve problems that use logic, sets, and functions.
- Solve problems using Boolean algebra.
- Solve problems that use permutations and combinations.
- Solve problems that use discrete probability.
- Solve problems that use basic graph theory.


TODO: for CSCI 432, add:

TOOD: for CSCI 535, add:  
* Explain the basics in topology, as they apply to computing with data.
* Compute Betti number, topological persistence, homology cycles, Reeb graphs from data.
* Read recent research papers in the area of computational topology.
* Articulate, both orally and in writing, mathematical proofs.
* Demonstrate teamwork skills.
* Present and critique applications of research in Topological Data Analysis (TDA).
* Recognize potential applications of TDA.


## When and Where?

* When? TODO  
* Where? TODO 

## How do I contact you?

* TODO: The preferred method for asking quiestions in this class is TODO
* If you would prefer to email Prof. Fasy, please use: brittany.fasy@montana.edu.  

Office hours: 

* Prof. Fasy: TODO, and by appointment.
* TA: TODO 

## Land Acknowledgement

Living in Montana, we are on the ancestral lands of American Indians, including
the 12 tribal nations that call Montana home today: A’aninin (Gros Ventre),
Amskapi/Piikani (Blackfeet), Annishinabe (Chippewa/Ojibway), Annishinabe/Métis
(Little Shell Chippewa), Apsáalooke (Crow), Ktunaxa/Ksanka (Kootenai), Lakota,
Dakota (Sioux), Nakoda (Assiniboine), Ne-i-yah-wahk (Plains Cree), Qíispé (Pend
d’Oreille), Seliš (Salish), and Tsétsêhéstâhese/So’taahe (Northern Cheyenne).
We honor and respect these tribal nations as we live, work, learn, and play in
this state.

To learn more about Montana Indians, I suggest starting with the following
pamphlet:
[Essential Understandings Regarding Montana
Indians](http://opi.mt.gov/Portals/182/Page%20Files/Indian%20Education/Indian%20Education%20101/essentialunderstandings.pdf)


## What is in this repository?

The folders in this repository contain all materials for this class.

- lec_notes: Copies of lecture notes and board photos.
- hw: homework assignments, as well as a LaTex template for your submissions. 

The schedule is at the bottom of this Markdown file.  If you want to learn more
about Markdown, check out [this tutorial](https://www.markdowntutorial.com/).

## Accessing this Repo

The repository is set as public, so you can access all course materials easily.
I suggest creating a fork, so that you can use your fork to maintain your own
materials for this class.  See the resources section below for forking directions.

To clone this repo:
```
$ git clone https://bitbucket.org/msu-cs/TODO.git
```
 

## Grading
Your grade for this class will be determined by: TODO

- 5% WebAssign
- 5% Class Participation
- 30% Homework
- 10% Online Quizzes
- 20% Midterm I
- 20% Midterm II
- X% Project
- 15% Final

TODO: Minimum passing grade requirement

## Class Policies

### Policy on Class Attendance

Class attendance and participation is required.  Attendance will be randomly
taken throughout the semester.

### Policy on Homework

All assignments must be submitted by 23:59 on the due date. Late
assignments will not be accepted.

Throughout this semester, we will have 8 homework assignments.

For descriptive assignments and reports, the submission should be typeset in
LaTex, and submitted as a PDF both in D2L and Gradescope. Each problem should be
started on a fresh page.

***Do not search for answers to the problems.*** You will learn in this class by
solving the problems. Regurgitating solutions you found elsewhere will not help
you learn the material.  If you feel that you need additional resources, please
ask for the instructor or TA.

For code assignments,
well organized source code with clear comments should be submitted.

### Policy on Collaboration

Collaboration is encouraged on all aspects of the class, except where explicitly
forbidden. Note:

- All collaboration (who and what) must be clearly indicated in writing on
  anything turned in.
- Homework may be solved collaboratively except as explicitly forbidden, but
  solutions must be written up **independently**.  This is best done by writing
  your solutions when not in a group setting.  Groups should be small enough
  that each member plays a significant role.  (Note, if there is a group assignment,
  each group is treated as an 'individual'.

### In-Person Classroom Etiquette

Except for note taking and group work requiring a computer, please keep electronic devices off during
class, as they can be distractions to other students. Disruptions to the class will
result in being asked to leave the lecture, and one half-point will be deducted
from the attendance grade.

### Zoom Classroom Etiquette

Please come to class prepared by reading and working on homework problems
throughout the week.  We welcome questions!  In the Zoom breakout rooms, we
expect that you are actively working on problems.  If needed, start your group
by recapping what was discussed right before going into the breakout room or
during the class period before.  If something is unclear, ask for a
clarification from your classmate.  If your group is unsure of what the task is,
please ask and do not sit idle!

### Withdrawing

After TODO:DATE, I will only support requests to withdraw from this course
with a ``W" grade if extraordinary personal circumstances exist.
If you are considering withdrawing from this class, discussing this with me as
early as possible is advised.  Since this class involves a project, the
decision to withdraw must be discussed with me, and with your group.

### Special Needs Information

If you have a documented disability for which you are or may be requesting an
accommodation(s), please contact me and Disabled
Student Services within the first two weeks of class.

### Diversity Statement

Montana State University considers the diversity of its students, faculty, and
staff to be a strength and critical to its educational mission. MSU expects
every member of the university community to contribute to an inclusive and
respectful culture for all in its classrooms, work environments, and at campus
events.  Dimensions of diversity can include sex, race, age, national origin,
ethnicity, gender identity and expression, intellectual and physical ability,
sexual orientation, income, faith and non-faith perspectives, socio-economic
status, political ideology, education, primary language, family status, military
experience, cognitive style, and communication style. The individual
intersection of these experiences and characteristics must be valued in our
community.

If there are aspects of the design, instruction, and/or experiences within this
course that result in barriers to your inclusion or accurate assessment of
achievement, please notify the instructor as soon as possible and/or contact
Disability Services or the Office of Institutional Equity.

### Taking a Class during the COVID-19 Pandemic

This class will be held primarily in person.  However, you (or your
family/friends) might become ill with COVID-19 or something else, which can make
even remote attendance difficult.  If this happens, please communicate with the
instructor as soon as possible to minimize impact on your academic experience.
Also, we welcome feedback throughout the semester about what is working well /
not working well.  Doing so will help this be a better experience for all!


## MSU Policies

### Academic Integrity

The integrity of the academic process requires that credit be given where credit
is due. Accordingly, it is academic misconduct to present the ideas or works of
another as one's own work, or to permit another to present one's work without
customary and proper acknowledgment of authorship. Students may collaborate with
other students only as expressly permitted by the instructor. Students are
responsible for the honest completion and representation of their work, the
appropriate citation of sources and the respect and recognition of others'
academic endeavors.

Plagiarism will not be tolerated in this course. According to the Meriam-Webster
dictionary, plagiarism is `the act of using another person's words or ideas
without giving credit to that person.'  Proper credit means describing all
outside resources (conversations, websites, etc.), and explaining the extent to
which the resource was used.  Penalties for plagiarism at MSU include (but are
not limited to) failing the assignment, failing the class, or having your degree
revoked.  This is serious, so do not plagiarize.
Even inadvertent or unintentional misuse or appropriation of another's work
(such as relying heavily on source material that is not expressly acknowledged)
is considered plagiarism. 

By participating in this class, you agree to abide by the Student Code of
Conduct.  This includes the following academic expectations:

- be prompt and regular in attending classes;
- be well-prepared for classes;
- submit required assignments in a timely manner;
- take exams when scheduled, unless rescheduled under 310.01;
- act in a respectful manner toward other students and the instructor and in a way
          that does not detract from the learning experience; and
- make and keep appointments when necessary to meet with the instructor. 


## MSU Drug and Alcohol Policies

```
Per the Code of Conduct for students, no student may come to class under the
influence of drugs or alcohol, as that would not be `Fostering a healthy, safe
and productive campus and community.`  See [Alcohol and Drug Policies
Website](http://www.montana.edu/deanofstudents/alcoholanddrugs.html) for more
information.  In particular, note:

As a federally-funded institution, we must adhere to all federal laws when it
comes to alcohol and drug use or distribution. This holds true for marijuana as
well. Using or distributing marijuana on or off campus is a violation of our
code of conduct even if a student has a medical card or comes from a state in
which marijuana is legal or has been decriminalized.

As noted, the University's alcohol and drug policies apply off campus. Using
drugs and/or alcohol and returning to your residence hall in a disruptive
fashion- either via odor, noise, destruction, etc- can lead to residence life
policy and alcohol or drug policy violations. Remember, not everyone wants to
hear or smell you.
```

## Resources

### Technical Resources

- [Git Udacity
  Course](https://www.udacity.com/course/how-to-use-git-and-github--ud775)
- [Forking in Git](https://help.github.com/articles/fork-a-repo/)
- [Markdown](http://daringfireball.net/projects/markdown/)
- [More Markdown](https://www.markdowntutorial.com/)
- [Inkscape Can Tutorial](http://tavmjong.free.fr/INKSCAPE/MANUAL/html/SoupCan.html)
- [Plagiarism Tutorial](http://www.lib.usm.edu/legacy/plag/pretest_new.php)]
- [Ott's 10 Tips](http://www.ms.uky.edu/~kott/proof_help.pdf)
- [Big-O, Intuitive Explanation](https://rob-bell.net/2009/06/a-beginners-guide-to-big-o-notation/)

### Course Textbook(s) 

- TODO


## Schedule

### Week 1 (TODO: dates)

- Topics: TODO 
- Reading: TODO 
- Online Quiz: TODO 

### Finals Week

- TODO: give finals information

--- 

This syllabus was created, using wording from previous courses that I have
taught, as well as from courses taught by my collaborators.  Thanks, all, for
making course content and syllabi publicly available!
All content in this repository is licensed under a [Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International License](This work is
licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0
International License.)
